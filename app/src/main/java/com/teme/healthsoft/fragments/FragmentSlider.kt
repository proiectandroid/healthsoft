package com.teme.healthsoft.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.teme.healthsoft.R
import kotlinx.android.synthetic.main.fragment_slider.*

/**
 * A simple [Fragment] subclass.
 */
class FragmentSlider : Fragment() {


    var pageTitle: String = ""
    var img = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_slider, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tv_title.text = pageTitle
        image.setImageResource(img)
    }

    fun setTitle(title: String){
        pageTitle = title
    }

    fun setImage(image: Int){
        img = image
    }

}
