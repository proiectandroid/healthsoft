package com.teme.healthsoft.fragments

import android.os.Bundle
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.teme.healthsoft.R
import kotlinx.android.synthetic.main.fragment_sign_up.*

class FragmentSignUp : Fragment() {

    companion object {
        fun newInstance() = FragmentSignUp()
    }

    private lateinit var mAuth: FirebaseAuth

    val transaction = fragmentManager?.beginTransaction()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_up, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mAuth = FirebaseAuth.getInstance()

        btn_sign_up.setOnClickListener {
            registerUser()
        }

        btn_already_acc.setOnClickListener {
            goToSignIn()
        }
    }

    private fun registerUser() {
        var email = et_email.text.toString().trim()
        var password = et_password.text.toString().trim()

        if (email.isEmpty()) {
            et_email.error = "Email is required!"
            et_email.requestFocus()
            return
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            et_email.error = "Please enter a valid email!"
            et_email.requestFocus()
            return
        }

        if (password.isEmpty()) {
            et_password.error = "Password is required!"
            et_password.requestFocus()
            return
        }

        if (password.length < 6) {
            et_password.error = "Minimum length of password should be 6"
            et_password.requestFocus()
            return
        }

        progress_bar.visibility = View.VISIBLE
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener { task ->
            progress_bar.visibility = View.GONE
            if (task.isSuccessful) {
                val user: FirebaseUser? = mAuth.currentUser
                user?.sendEmailVerification()?.addOnCompleteListener { task1 ->
                    if (task1.isSuccessful) {
                        Toast.makeText(activity, "User registered!", Toast.LENGTH_SHORT).show()
                        goToSignIn()
                    }
                }
            }
        }
    }

    private fun goToSignIn() {
        val manager: FragmentManager = activity!!.supportFragmentManager
        manager.popBackStack()
    }
}
