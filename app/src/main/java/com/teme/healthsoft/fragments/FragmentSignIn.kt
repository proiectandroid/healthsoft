package com.teme.healthsoft.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import com.teme.healthsoft.R
import com.teme.healthsoft.activities.DashboardActivity
import kotlinx.android.synthetic.main.fragment_sign_in.*

/**
 * A simple [Fragment] subclass.
 */
class FragmentSignIn : Fragment() {

    companion object {
        fun newInstance() = FragmentSignIn()
    }

    lateinit var gso: GoogleSignInOptions

    lateinit var mAuth:FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_in, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mAuth = FirebaseAuth.getInstance()
        gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        btn_sign_in.setOnClickListener {
            signInUser()
        }

        btn_no_acc.setOnClickListener {
            goToSignUp()
        }

        tv_forgot_pw.setOnClickListener {
            goToForgotPw()
        }
    }

    private fun signInUser(){
        var email = et_email.text.toString().trim()
        var password = et_password.text.toString().trim()

        if(email.isEmpty()){
            et_email.error = "Email is required!"
            et_email.requestFocus()
            return
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            et_email.error = "Please enter a valid email!"
            et_email.requestFocus()
            return
        }

        if(password.isEmpty()){
            et_password.error = "Password is required!"
            et_password.requestFocus()
            return
        }

        if(password.length < 6){
            et_password.error = "Minimum length of password should be 6"
            et_password.requestFocus()
            return
        }
        progress_bar.visibility=View.VISIBLE
        btn_sign_in.isEnabled = false
        Toast.makeText(activity, "Please, wait", Toast.LENGTH_SHORT)
            .show()
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener { task ->

            if(task.isSuccessful)
            {
                val user = mAuth.currentUser
                if(user != null) {
                    if (user!!.isEmailVerified) {
                        startActivity(Intent(activity, DashboardActivity::class.java))
                        activity!!.finish()
                    } else {
                        Toast.makeText(activity, "Please, verify your email!", Toast.LENGTH_SHORT)
                            .show()
                    }
                }

            }
            else
            {
                Toast.makeText(activity, "Wrong email or password!", Toast.LENGTH_SHORT).show()
                progress_bar.visibility=View.GONE
                btn_sign_in.isEnabled = true
            }

        }
    }

    private fun goToSignUp()
    {
        val fragment = FragmentSignUp.newInstance()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.replace(R.id.fl_container, fragment, "FragmentSignUp")
        transaction?.addToBackStack("FragmentSignUp")
        transaction?.commit()
    }

    private fun goToForgotPw()
    {
        val fragment = FragmentForgotPassword.newInstance()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.replace(R.id.fl_container, fragment, "FragmentForgotPassword")
        transaction?.addToBackStack("FragmentForgotPassword")
        transaction?.commit()
    }
}
