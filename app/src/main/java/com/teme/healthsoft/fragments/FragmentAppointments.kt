package com.teme.healthsoft.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.teme.healthsoft.models.Appointment
import com.teme.healthsoft.adapters.AppointmentAdapter
import com.teme.healthsoft.adapters.OnAppointmentItemClickListener
import com.teme.healthsoft.R
import kotlinx.android.synthetic.main.fragment_appointments.*
import java.util.*


class FragmentAppointments : Fragment(),
    OnAppointmentItemClickListener {

    lateinit var ref: DatabaseReference
    var firebaseAuth = FirebaseAuth.getInstance()
    var user = firebaseAuth.currentUser
    var appointmentList: ArrayList<Appointment> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_appointments, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ref = FirebaseDatabase.getInstance().getReference("Appointments")

        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(dataSnapshot: DatabaseError) {
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    appointmentList.clear()
                    for (a in dataSnapshot.children) {
                        val appointment = a.getValue(Appointment::class.java)
                        if (appointment != null && appointment.patientId == user?.uid) {
                            appointmentList.add(appointment)
                        }
                    }
                    rv_appointments.layoutManager = LinearLayoutManager(activity!!)
                    rv_appointments.adapter =
                        AppointmentAdapter(
                            appointmentList,
                            this@FragmentAppointments
                        )
                    progress_bar.visibility = View.GONE
                    btn_new_appointment.isEnabled = true
                }
            }
        })

        btn_new_appointment.setOnClickListener {
            val transaction = activity?.supportFragmentManager?.beginTransaction()
            transaction?.replace(R.id.fl_container, FragmentNewAppointment(), "FragmentHome")
            transaction?.addToBackStack("FragmentHome")
            transaction?.commit()
        }
    }

    override fun onItemClick(appointment: Appointment, position: Int) {
        deleteApp(appointment.id)
        appointmentList.removeAt(position)
        rv_appointments.adapter?.notifyDataSetChanged()
        Toast.makeText(activity, "Appointment deleted", Toast.LENGTH_SHORT).show()
    }

    private fun deleteApp(appId: String) {
        ref = FirebaseDatabase.getInstance().getReference("Appointments").child(appId)
        ref.removeValue()
    }
}
