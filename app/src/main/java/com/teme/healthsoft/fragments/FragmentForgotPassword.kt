package com.teme.healthsoft.fragments

import android.os.Bundle
import android.util.Patterns
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import com.google.firebase.auth.FirebaseAuth
import com.teme.healthsoft.R
import kotlinx.android.synthetic.main.fragment_forgot_password.*
import kotlinx.android.synthetic.main.fragment_forgot_password.et_email


class FragmentForgotPassword : Fragment() {

    companion object {
        fun newInstance() =
            FragmentForgotPassword()
    }

    lateinit var mAuth: FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_forgot_password, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mAuth = FirebaseAuth.getInstance()

        btn_get_mail.setOnClickListener {
            getMail()
        }

        btn_back.setOnClickListener {
            goToSignIn()
        }
    }

    private fun getMail() {
        var email = et_email.text.toString().trim()

        if (email.isEmpty()) {
            et_email.error = "Email is required!"
            et_email.requestFocus()
            return
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            et_email.error = "Please enter a valid email!"
            et_email.requestFocus()
            return
        }

        mAuth.sendPasswordResetEmail(email).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                Toast.makeText(activity, "Email will be sent shortly!", Toast.LENGTH_SHORT).show()
                goToSignIn()
            }
        }
    }

    private fun goToSignIn()
    {
        val manager: FragmentManager = activity!!.supportFragmentManager
        manager.popBackStack()
    }
}
