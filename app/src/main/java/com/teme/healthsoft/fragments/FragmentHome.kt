package com.teme.healthsoft.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager

import com.teme.healthsoft.R
import com.teme.healthsoft.adapters.NewsAdapter
import com.teme.healthsoft.models.Article
import com.teme.healthsoft.models.Source
import kotlinx.android.synthetic.main.fragment_home.*
import okhttp3.*
import org.json.JSONObject
import java.io.IOException

/**
 * A simple [Fragment] subclass.
 */
class FragmentHome : Fragment() {

    var articleList = ArrayList<Article>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recycler_view.layoutManager = LinearLayoutManager(activity)

        syncWithServer()
    }

    private fun syncWithServer(){
        val url = "https://newsapi.org/v2/top-headlines?country=us&apiKey=5ef7d79a42f14f68816733282f4d9663"

        val request = Request.Builder().url(url).build()

        val client = OkHttpClient()

        client.newCall(request).enqueue(object : Callback{
            override fun onFailure(call: Call, e: IOException) {
                Log.d("Nu","Bine")
            }

            override fun onResponse(call: Call, response: Response) {
                val body = response.body()?.string()
                println(body)

                val news = JSONObject(body!!)

                val articlesArray = news.getJSONArray("articles")

                articleList.clear()
                for(i  in 0..articlesArray.length() - 1){
                    val articleObj = articlesArray.getJSONObject(i)

                    val author = articleObj.getString("author")
                    val content = articleObj.getString("content")
                    val description = articleObj.getString("description")
                    val publishedAt = articleObj.getString("publishedAt")
                    val title = articleObj.getString("title")
                    val url = articleObj.getString("url")
                    val urlToImage = articleObj.getString("urlToImage")

                    val sourceObj = articleObj.getJSONObject("source")
                    val id = sourceObj.get("id")
                    val name = sourceObj.getString("name")
                    val source = Source(id, name)

                    val article = Article(author, content, description, publishedAt, source, title, url, urlToImage)

                    articleList.add(article)
                }
                activity!!.runOnUiThread{
                    recycler_view.adapter = NewsAdapter(articleList)
                }

            }

        })
    }

}
