package com.teme.healthsoft.fragments

import android.app.DatePickerDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.teme.healthsoft.models.Patient

import com.teme.healthsoft.R
import kotlinx.android.synthetic.main.fragment_update_profile.*
import kotlinx.android.synthetic.main.nav_header.*
import java.util.*
import kotlin.collections.ArrayList

/**
 * A simple [Fragment] subclass.
 */
class FragmentUpdateProfile : Fragment() {

    private val cal = Calendar.getInstance()

    lateinit var ref : DatabaseReference
    var firebaseAuth = FirebaseAuth.getInstance()
    var user = firebaseAuth.currentUser


    var patientList: ArrayList<Patient> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_update_profile, container, false)



    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ref = FirebaseDatabase.getInstance().getReference("Patients")
        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (p in dataSnapshot.children) {
                        val patient = p.getValue(Patient::class.java)
                        if (patient != null) {
                            patientList.add(patient)
                        }
                    }
                    getInfo()
                    btn_submit.isEnabled = true
                    progress_bar.visibility = View.GONE
                }
            }
        })

        btn_submit.setOnClickListener {
            submit()
        }


        btn_set_date.setOnClickListener {
            val dateSetListener =
                DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                    cal.set(Calendar.YEAR, year)
                    cal.set((Calendar.MONTH+1), month)
                    cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                    et_date.text = "$dayOfMonth/$month/$year"
                }
            DatePickerDialog(
                context!!,
                dateSetListener,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
            ).show()
        }
    }

    private fun getInfo() {
        for(p in patientList)
        {
            if(p.id == user!!.uid)
            {
                et_first_name.setText(p.firstName)
                et_last_name.setText(p.lastName)
                et_number.setText(p.phoneNumber)
                et_adress.setText(p.adress)
                et_date.setText(p.dateOfBirth)
                et_gender.setText(p.gender)
            }
        }
    }

    private fun submit() {
        var firstName = et_first_name.text.toString().trim()
        var lastName = et_last_name.text.toString().trim()
        var number = et_number.text.toString().trim()
        var adress = et_adress.text.toString().trim()
        var dateOfBirth = et_date.text.toString().trim()
        var gender = et_gender.text.toString().trim()

        if(firstName.isEmpty()){
            et_first_name.error = "First name is required!"
            et_first_name.requestFocus()
            return
        }
        if(lastName.isEmpty()){
            et_last_name.error = "Last name is required!"
            et_last_name.requestFocus()
            return
        }
        if(number.isEmpty()){
            et_number.error = "Phone number is required!"
            et_number.requestFocus()
            return
        }
        if(adress.isEmpty()){
            et_adress.error = "Adress is required!"
            et_adress.requestFocus()
            return
        }
        if(dateOfBirth == "Date of birth:"){
            et_date.error = "Date of birth is required!"
            et_date.requestFocus()
            return
        }
        if(gender.isEmpty()){
            et_gender.error = "Gender is required!"
            et_gender.requestFocus()
            return
        }


        val patientId = user!!.uid
        var email = activity!!.tv_email.text.toString()
        var patient = Patient(
            patientId,
            firstName,
            lastName,
            number,
            adress,
            dateOfBirth,
            gender,
            email
        )
        if (patientId != null) {
            ref.child(patientId).setValue(patient).addOnCompleteListener {
                Toast.makeText(activity, "Patient added successfully", Toast.LENGTH_SHORT).show()
            }
        }
    }

}
