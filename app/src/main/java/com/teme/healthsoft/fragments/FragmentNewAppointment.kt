package com.teme.healthsoft.fragments

import android.app.*
import android.content.Context

import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TimePicker
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationManagerCompat
import androidx.fragment.app.FragmentManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.teme.healthsoft.models.Appointment

import com.teme.healthsoft.R
import com.teme.healthsoft.notification.Broadcast
import kotlinx.android.synthetic.main.fragment_new_appointment.*
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class FragmentNewAppointment : Fragment() {

    companion object {
        fun newInstance() = FragmentNewAppointment()
    }

    lateinit var ref : DatabaseReference
    var firebaseAuth = FirebaseAuth.getInstance()
    var user = firebaseAuth.currentUser
    private val cal = Calendar.getInstance()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_appointment, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        createNotificationChannel()
        btn_submit.setOnClickListener {
            submit()
        }

        btn_set_date.setOnClickListener {
            val dateSetListener =
                DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                    cal.set(Calendar.YEAR, year)
                    cal.set((Calendar.MONTH), month)
                    cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                    tv_date.text = "$dayOfMonth/$month/$year"
                }
            DatePickerDialog(
                context!!,
                dateSetListener,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

        btn_set_hour.setOnClickListener {
            val timeSetListener =
                TimePickerDialog.OnTimeSetListener { view: TimePicker?, hourOfDay: Int, minute: Int ->
                    cal.set(Calendar.HOUR_OF_DAY, hourOfDay)
                    cal.set(Calendar.MINUTE, minute)
                    tv_hour.text = SimpleDateFormat("HH:mm").format(cal.time)
                }
            TimePickerDialog(
                activity,
                timeSetListener,
                cal.get(Calendar.HOUR_OF_DAY),
                cal.get(Calendar.MINUTE),
                true
            ).show()
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun submit() {
        var name = et_name.text.toString().trim()
        var date = tv_date.text.toString().trim()
        var hour = tv_hour.text.toString().trim()
        var description = et_description.text.toString().trim()

        if(name.isEmpty()){
            et_name.error = "First name is required!"
            et_name.requestFocus()
            return
        }
        if(description.isEmpty()){
            et_description.error = "Last name is required!"
            et_description.requestFocus()
            return
        }
        if(date == "Date of appointment:"){
            Toast.makeText(activity, "Please, insert a date!", Toast.LENGTH_SHORT).show()
            return
        }
        if(hour == "Time of appointment:"){
            Toast.makeText(activity, "Please, insert a time!", Toast.LENGTH_SHORT).show()
            return
        }

        ref = FirebaseDatabase.getInstance().getReference("Appointments")
        val patientId = user!!.uid
        var appointmentId:String = ref.push().key.toString()

        var appointment: Appointment =
            Appointment(
                appointmentId,
                patientId,
                name,
                description,
                date,
                hour
            )
        if (appointmentId != null) {
            ref.child(appointmentId).setValue(appointment).addOnCompleteListener {
                setAlarm()
                Toast.makeText(context, "Appointment made successfully", Toast.LENGTH_SHORT).show()
                val manager: FragmentManager = activity!!.supportFragmentManager
                manager.popBackStack()
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun setAlarm()
    {
        val intent = Intent(activity, Broadcast::class.java)
        val pendingIntent : PendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0)
        val alarmManager: AlarmManager = context?.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val second = Calendar.getInstance().get(Calendar.SECOND) * 1000
        val time = cal.timeInMillis - second
        alarmManager.set(AlarmManager.RTC_WAKEUP, time , pendingIntent)

    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel() {
        val channel = NotificationChannel("notify", "Channel", NotificationManager.IMPORTANCE_DEFAULT)
        val notificationManager: NotificationManager = activity?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
    }
}
