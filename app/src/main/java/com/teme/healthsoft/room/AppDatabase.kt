package com.teme.healthsoft.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Doctor::class], version = 1)
abstract class AppDatabase : RoomDatabase(){

    abstract fun doctorDao(): DoctorDao

    companion object{
        private var mInstance : AppDatabase? = null

        fun getAppDatabase(context: Context): AppDatabase {
            if(mInstance == null){
                mInstance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "doctor"
                ).build()
            }
            return mInstance as AppDatabase
        }
    }

}