package com.teme.healthsoft.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
public interface DoctorDao {

    @androidx.room.Query("SELECT * FROM doctor")
    fun getAll(): List<Doctor>

    @Insert
    fun insert(doctor: Doctor)

    @Query("DELETE from doctor")
    fun deleteAll()

}