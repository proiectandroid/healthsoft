package com.teme.healthsoft.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.squareup.picasso.Picasso
import com.teme.healthsoft.R
import com.teme.healthsoft.models.Article
import kotlinx.android.synthetic.main.news_view.view.*

class NewsAdapter(private val articles: ArrayList<Article>) : RecyclerView.Adapter<NewsAdapter.CustomViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.news_view, parent, false)
        return CustomViewHolder(view)
    }

    override fun getItemCount(): Int = articles.size

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.bind(articles[position])
    }

    inner class CustomViewHolder(private val view: View) : RecyclerView.ViewHolder(view){

        fun bind(article: Article){
            view.tv_author.text = article.author
            view.tv_source.text = article.source.name
            view.tv_title.text = article.title
            view.tv_desc.text = article.description
            view.tv_published_at.text = article.publishedAt
            Glide.with(view.context).load(article.urlToImage).into(view.image_view)
            view.progress_bar.visibility = View.GONE
        }
    }
}