package com.teme.healthsoft.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import com.teme.healthsoft.R
import com.teme.healthsoft.room.Doctor
import kotlinx.android.synthetic.main.doctors_view.view.*

class DoctorsAdapter(private val doctorList: ArrayList<Doctor>, var clickListener: OnDoctorItemClickListener) :
    RecyclerView.Adapter<DoctorsAdapter.CustomViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.doctors_view, parent, false)
        return CustomViewHolder(view)
    }

    override fun getItemCount(): Int = doctorList.size

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.initialize(doctorList[position], clickListener)
    }

    inner class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(doctor: Doctor) {
            val firstName = doctor.firstName
            val lastName = doctor.lastName
            view.tv_name.text = "$firstName $lastName"
            view.tv_specialization.text = doctor.specialization
            view.tv_description.text = doctor.description
            view.tv_email.text = doctor.email
            val docImage = view.image_view
            Picasso.with(view.context).load(doctor.imageUrl).into(docImage, object : Callback {
                override fun onSuccess() {
                    Log.d("Succes", "Poza")
                }

                override fun onError() {
                    Log.d("Error", "Poza")
                }

            })
        }

        fun initialize(doctor: Doctor, action: OnDoctorItemClickListener){
            val firstName = doctor.firstName
            val lastName = doctor.lastName
            view.tv_name.text = "$firstName $lastName"
            view.tv_specialization.text = doctor.specialization
            view.tv_description.text = doctor.description
            view.tv_email.text = doctor.email
            val docImage = view.image_view
            Picasso.with(view.context).load(doctor.imageUrl).into(docImage, object : Callback {
                override fun onSuccess() {
                    Log.d("Succes", "Poza")
                }

                override fun onError() {
                    Log.d("Error", "Poza")
                }

            })
            view.image_view_email.setOnClickListener{
                action.onItemClick(doctor, adapterPosition)
            }
        }
    }
}

interface OnDoctorItemClickListener{
    fun onItemClick(doctor: Doctor, position: Int)
}