package com.teme.healthsoft.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class PagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {

    val list: MutableList<Fragment> = ArrayList()

    override fun getItem(position: Int) = list[position]

    override fun getCount() = list.size

}