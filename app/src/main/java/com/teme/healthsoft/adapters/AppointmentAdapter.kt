package com.teme.healthsoft.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.teme.healthsoft.models.Appointment
import com.teme.healthsoft.R
import kotlinx.android.synthetic.main.appointment_view.view.*



class AppointmentAdapter(private val appointmentList : ArrayList<Appointment>, var clickListener: OnAppointmentItemClickListener) : RecyclerView.Adapter<AppointmentAdapter.CustomViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.appointment_view, parent, false)
        return CustomViewHolder(view)
    }

    override fun getItemCount(): Int = appointmentList.size

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.bind(appointmentList[position], clickListener)
    }

    inner class CustomViewHolder(private val view: View) : RecyclerView.ViewHolder(view){

        fun bind(app: Appointment, action: OnAppointmentItemClickListener){
            val name = app.doctor
            val date = app.date
            val hour = app.hour
            view.tv_name.text = name
            view.tv_date.text = date
            view.tv_hour.text = hour
            view.btn_delete.setOnClickListener {
                action.onItemClick(app, adapterPosition)
            }
        }
    }
}

interface OnAppointmentItemClickListener{
    fun onItemClick(appointment: Appointment, position: Int)
}