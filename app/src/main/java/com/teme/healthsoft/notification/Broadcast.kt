package com.teme.healthsoft.notification

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.teme.healthsoft.R

class Broadcast : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val builder: NotificationCompat.Builder = NotificationCompat.Builder(context!!, "notify")
            .setContentTitle("Appointment").setContentText("You have an appointment now")
            .setSmallIcon(
                R.drawable.ic_action_notification
            ).setPriority(NotificationCompat.PRIORITY_DEFAULT)
        val notificationManager: NotificationManagerCompat = NotificationManagerCompat.from(context)
        notificationManager.notify(200, builder.build())

    }
}