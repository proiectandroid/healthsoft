package com.teme.healthsoft.models

data class Source(
    val id: Any,
    val name: String
)