package com.teme.healthsoft.models

class Patient(var id: String? = "",
              var firstName: String = "",
              var lastName: String = "",
              var phoneNumber: String = "",
              var adress: String = "",
              var dateOfBirth: String = "",
              var gender: String = "",
              var email: String = "")
{

}
